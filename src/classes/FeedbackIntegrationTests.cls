@isTest
public with sharing class FeedbackIntegrationTests {
	

	@testSetup
	static void setup(){
		User manager = UserDataFactory.createManager('Silvio');
		insert manager;
		User user1 = UserDataFactory.createManager('Alexandra');
		insert user1;
		User user2 = UserDataFactory.createManager('Luis');
		user2.ManagerId = manager.Id;
		insert user2;
		User user3 = UserDataFactory.createManager('Liz');
		insert user3;
		User subuser = UserDataFactory.createManager('Simone');
		subuser.ManagerId = user3.Id;
		insert subuser;
	}


	@isTest
	static void  TestaACriacaoDeUmFeedbackCorreto() {
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, user2,  '4',  '5',  '1');
			insert fb1;

		    Feedback__c fb1Inserted = [SELECT Id, 
					    Feedback_From__c, 
					    Feedback_To__c, 
					    Communication_Skills__c, 
					    Commitment__c, 
					    Leadership_Skills__c 
					FROM Feedback__c LIMIT 1];

			System.assertEquals(fb1.Id, fb1Inserted.Id);
			System.assertEquals('4', fb1Inserted.Communication_Skills__c);
			System.assertEquals('5', fb1Inserted.Commitment__c);
			System.assertEquals('1', fb1Inserted.Leadership_Skills__c);
			System.assertEquals(user1.Id, fb1Inserted.Feedback_From__c);
			System.assertEquals(user2.Id, fb1Inserted.Feedback_To__c);
		}
	}

	@isTest
	static void  TestaACriacaoDeFeedbacksIncorretos() {
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		System.runAs(user1){
			//Os campos de avaliacao sao obrigatorios
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, user2, '', '5',  '1');
			try{
				insert fb1;
			}catch(DmlException	ex){
				System.assertEquals(null, fb1.Id);
			}

			Feedback__c fb2 = FeedbackDataFactory.createFeedback(user1, user2, '1', '',  '1');
			try{
				insert fb2;
			}catch(DmlException	ex){
				System.assertEquals(null, fb2.Id);
			}

			Feedback__c fb3 = FeedbackDataFactory.createFeedback(user1, user2, '1', '5',  '');
			try{
				insert fb3;
			}catch(DmlException	ex){
				System.assertEquals(null, fb3.Id);
			}

			//O usuario 1 não pode criar um feedback para o usuario 2.
			//O usuario só pode criar feedbacks onde ele seja o valor do campo Feedback_From__c
			Feedback__c fb4 = FeedbackDataFactory.createFeedback(user2, user1, '1', '5',  '');
			try{
				insert fb4;
			}catch(DmlException	ex){
				System.assertEquals(null, fb4.Id);
			}

		}
	}

	@isTest
	static void TestaQueOFeedbackEstaVisivelApenasParaORemetenteDestinatarioESeuGerente(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];
		User subuser = [SELECT Id FROM User WHERE Username = 'Simone@john.com.br'];
		User manager = [SELECT Id FROM User WHERE Username = 'Silvio@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, subuser,  '4',  '5',  '1');
			insert fb1;
		}

		//pode ver porque é o gerente do destinatário
		System.runAs(user2){
			Feedback__c fb1_Inserted = [SELECT Id FROM Feedback__c LIMIT 1];
			System.assertNotEquals(null, fb1_Inserted);
		}

		//pode ver porque é o destinatario
		System.runAs(subuser){
			Feedback__c fb1_Inserted = [SELECT Id FROM Feedback__c LIMIT 1];
			System.assertNotEquals(null, fb1_Inserted);
		}

		//Nao pode ver, mesmo estando acima da hierarquia
		System.runAs(manager){
			Feedback__c fb1_Inserted = null;
			try{
				fb1_Inserted = [SELECT Id FROM Feedback__c LIMIT 1];
			}catch(QueryException ex){
				System.assertEquals(null, fb1_Inserted);
			}
		}
	}

	@isTest
	static void TestaQueORegistroEMandadoParaAprovacaoNoMomentoDaSuaCriacao(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, user2, '3', '5',  '1');
			insert fb1;

			ProcessInstance approvalProcess = [SELECT Id, Status, TargetObjectId, SubmittedById FROM ProcessInstance WHERE Status = 'Pending' LIMIT 1];
			System.assertNotEquals(null, approvalProcess);
			System.assertEquals(fb1.Id, approvalProcess.TargetObjectId);
			System.assertEquals(user1.Id, approvalProcess.SubmittedById);
		}
	}

	@isTest
	static void TestaQueNaoEPrecisoDeixarUmComentarioCasoOProcessoDeAprovacaoSejaAprovado(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		Feedback__c fb1 = null;
		System.runAs(user1){
			fb1 = FeedbackDataFactory.createFeedback(user1, user2, '3', '5',  '1');
			insert fb1;

			ProcessInstance approvalProcess = [SELECT Id, 
			 										  Status, 
			 										  TargetObjectId, 
			 										  SubmittedById 
			 								     FROM ProcessInstance 
			 								    WHERE Status = 'Pending' LIMIT 1];

			System.assertNotEquals(null, approvalProcess);
			System.assertEquals(fb1.Id, approvalProcess.TargetObjectId);
			System.assertEquals(user1.Id, approvalProcess.SubmittedById);
		}

		System.runAs(user2){
			//Aprova sem a necessidade de deixar um comentario
			Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
			req.setAction('Approve');

			ProcessInstanceWorkitem pItem = [SELECT Id 
												FROM ProcessInstanceWorkitem 
											   WHERE ProcessInstance.TargetObjectId =:fb1.id];

			req.setWorkitemId(pItem.Id);
			Approval.ProcessResult result = Approval.process(req);

			ProcessInstance approvalProcess = [SELECT Id, 
			 										  Status, 
			 										  TargetObjectId, 
			 										  SubmittedById 
			 								     FROM ProcessInstance 
			 								    WHERE Status = 'Approved' LIMIT 1];
			System.assertNotEquals(null, approvalProcess);
		}
	}

	@isTest
	static void TestaQueEPrecisoDeixarUmComentarioCasoOProcessoDeAprovacaoSejaRejeitado(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		Feedback__c fb1 = null;
		System.runAs(user1){
			fb1 = FeedbackDataFactory.createFeedback(user1, user2, '3', '5',  '1');
			insert fb1;

			ProcessInstance approvalProcess = [SELECT Id, 
			 										  Status, 
			 										  TargetObjectId, 
			 										  SubmittedById 
			 								     FROM ProcessInstance 
			 								    WHERE Status = 'Pending' LIMIT 1];

			System.assertNotEquals(null, approvalProcess);
			System.assertEquals(fb1.Id, approvalProcess.TargetObjectId);
			System.assertEquals(user1.Id, approvalProcess.SubmittedById);
		}

		System.runAs(user2){
			//Primeiro rejeita o processo sem deixar nenhum comentario
			Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
			req.setAction('Reject');

			ProcessInstanceWorkitem pItem = [SELECT Id 
												FROM ProcessInstanceWorkitem 
											   WHERE ProcessInstance.TargetObjectId =:fb1.id];

			req.setWorkitemId(pItem.Id);
			try{
				Approval.ProcessResult result = Approval.process(req);
			}catch(Exception ex){
				System.assertEquals('Process failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, It is necessary to add a Comment if you are rejecting this Feedback: []', ex.getMessage());
			}

			//agora adiciona o comentario e verifica que deu certo reprovar o processo
			req.setComments('Não Aceito!');
			Approval.ProcessResult result = Approval.process(req);
			ProcessInstance approvalProcess = [SELECT Id, 
			 										  Status, 
			 										  TargetObjectId, 
			 										  SubmittedById 
			 								     FROM ProcessInstance 
			 								    WHERE Status = 'Rejected' LIMIT 1];
			System.assertNotEquals(null, approvalProcess);
		}
	}

	@isTest
	static void TestaQueOCampoCommentsResponsePrecisaSerPreenchidoQuandoAcceptanceTemValorNo(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, user2, '3', '5',  '1');
			fb1.Feedback_Acceptance__c	 = 'No';
			try{
				insert fb1;
			}catch(DmlException	ex){
				System.assertEquals('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, If the Feedback Acceptance value is \'No\' you must leave a response in the Comments Response field.: [Comments_Response__c]', ex.getMessage());
			}
		}
	}

	@isTest
	static void TestaQueOCampoCommentsResponseNaoPrecisaDeValorQuandoAcceptanceTemValorYes(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, user2, '3', '5',  '1');
			fb1.Feedback_Acceptance__c	 = 'Yes';
			insert fb1;
		}
	}

	@isTest
	static void TestaQueNaoEPossivelEditarOFeedbackAposEnviarParaAprovacao(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, user2, '3', '5',  '1');
			insert fb1;
			try{
				update fb1;
			}catch(DmlException ex){
				System.assertEquals(true, Approval.isLocked(fb1.id));
			}
		}	
	}

	@isTest
	static void TestaQueNaoEPossivelDeletarOFeedbackAposEnviarParaAprovacao(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User user2 = [SELECT Id FROM User WHERE Username = 'Liz@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, user2, '3', '5',  '1');
			insert fb1;
			try{
				delete fb1;
			}catch(DmlException ex){
				System.assertEquals(true, Approval.isLocked(fb1.id));
			}
			
		}
	}


	@isTest
	static void TestaOEnvioDeEmailSemanalParaOGerentesDefinidosNosRegistrosDosUsuarios(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User subuser = [SELECT Id FROM User WHERE Username = 'Simone@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, subuser, '3', '5',  '1');
			Feedback__c fb2 = FeedbackDataFactory.createFeedback(user1, subuser, '3', '5',  '1');
			Feedback__c fb3 = FeedbackDataFactory.createFeedback(user1, subuser, '3', '5',  '1');
			insert fb1;
			insert fb2;
			insert fb3;
		}

		Test.startTest();
		//se nao acontecer nenhuma excessao entao o processo de envio de email ocorreu com sucesso
		try{
			FeedbackEmailDispatcherJob batch = new FeedbackEmailDispatcherJob();
			Database.executeBatch(batch);
		}catch(Exception ex){
			System.debug(ex.getMessage());
		}

		Test.stopTest();
	}


	@isTest
	static void TestaOAgendamentoDoServicoQueEnviaEmailSemanalParaOsGerentes(){
		User user1 = [SELECT Id FROM User WHERE Username = 'Alexandra@john.com.br'];
		User subuser = [SELECT Id FROM User WHERE Username = 'Simone@john.com.br'];

		System.runAs(user1){
			Feedback__c fb1 = FeedbackDataFactory.createFeedback(user1, subuser, '3', '5',  '1');
			Feedback__c fb2 = FeedbackDataFactory.createFeedback(user1, subuser, '3', '5',  '1');
			Feedback__c fb3 = FeedbackDataFactory.createFeedback(user1, subuser, '3', '5',  '1');
			insert fb1;
			insert fb2;
			insert fb3;
		}

		Test.startTest();
		//se nao acontecer nenhuma excessao entao o processo de envio de email ocorreu com sucesso
		String jobId = null;
		try{
			jobId = System.schedule('FeedbackEmailDispatcherSchedulableJob', '0 0 0 ? * FRI', new FeedbackEmailDispatcherSchedulableJob());
		}catch(Exception ex){
			System.debug(ex.getMessage());
		}

		
		Test.stopTest();

		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id =:jobId];
		System.assertEquals('0 0 0 ? * FRI', ct.CronExpression);
	}


}