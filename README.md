#Projeto Feedback Johson & Johson#

***Autor: Allan Oricil***

***Engenheiro de Computação pela Universidade Federal de Itajubá***

***Desenvolvedor Salesforce***


Este projeto é uma solução para o processo de avaliação por Feedbacks dentro de uma empresa. 
As imagens abaixo exemplificam passo a passo o processo de feedbacks neste sistema. Além disso
fiz esse projeto para não ficar preso a perfis e papéis. Então as configurações do objeto customizado Feedback estão como compartilhamento particular e com a visibilidade por papéis desabilitada. Para fazer deploy basta utilizar o conteúdo da pasta 'src'. Nessa pasta você encontrará todos os Metadados e o 'package.xml'.

Para atender a regra de compartilhamento exigida eu utilizei a tabela auxiliar Feedback_Share, criada automaticamente pelo Salesforce quando o OWD para o objeto é trocado para particular. Criando registros dentro do objeto Feedback_Share é possível dizer quem terá acesso de visibilidade para o registro. Então toda vez que um feedback é criado, o usuário inserido no campo Feedback To recebe o direito de visualizar o registro. Além disso, caso o usuário do campo Feedback To possua um Gerente, no caso o gerente colocado no seu registro de usuário e não o gerente na hierarquia, este gerente também ganha o acesso de visualizar o registro. Isso também foi feito inserindo um registro na tabela Feedback_Share. 

Outra coisa importante é que cada vez que um registro de Feedback é criado, um processo de aprovação é disparado. O usuário que recebeu o feedback se torna o aprovador e a primeira ação desse processo é enviar um email para quem recebeu o feedback. O usuário aprovador pode aprovar ou rejeitar o feedback. Caso ele reprove o feedback é preciso inserir um comentário. Caso ele tente reprovar e não insira um comentário, aparecerá o erro da imagem abaixo. Isso acontece porque a etapa de rejeição do processo de aprovação é atualizar o campo Feedback_Acceptance_c para 'No'. E toda vez que o valor para esse campo é 'No' é preciso inserir um valor para o campo Comments_Response_c. Caso quem recebeu o feedback opte por aprovar o feedback, não é necessário inserir um comentário. E caso ele aprove, a ação de aprovação irá atualizar o campo Feedback_Acceptance_c para 'Yes'. E quando o processo é aprovado ou reprovado, o usuário que criou o feedback recebe um email de notificação com a resposta de quem recebeu o feedback.

Por fim, existe ainda um serviço agendado que envia um email para todos os gerentes contendo os feedbacks dos seus subordinados. E lembre-se que nós estamos utilizando o campo Gerente do registro de usuário e não os papéis. Os modelos de email estão logo abaixo também.


##1 - Criação de Feedback##
![picture](/images/newFeedback.JPG)

##2 - Feedback Esperando Aprovação###
![picture](/images/feedbackEsperandoAprovacao.JPG)

##3 - Visão do Processo de Aprovação pelo usuário que recebeu Feedback###
![picture](/images/ProcessosDeAprovacao.JPG)

##4 - Visão do registro de Processo de Aprovação###
![picture](/images/processoDeAprovacaoVisaoDoUsuarioQueRecebeu.JPG)

##5 - Rejeição do Processo de Aprovação inserindo um comentário###
![picture](/images/rejeitaProcessoDeAprovacaoComComentario.JPG)

##6 - Feedback é atualizado com o comentário e com Feedback_Acceptance igual a No###
![picture](/images/feedbackAtualizadoComNaoAceitacaoEComentario.jpg)

##7 - Rejeição do Processo de Aprovação não inserindo um comentário###
![picture](/images/rejeicaoSemComentarioApareceErro.JPG)

##8 - Aprovação do Processo de Aprovação###
![picture](/images/aprovacaoDoProcessodeAprovacao.JPG)

##9 - Feedback é atualizado com o comentário e com Feedback_Acceptance igual a Yes###
![picture](/images/feedbackAtualizadoComAceitacaoEComentario.jpg)

##10 - Email enviado para quem recebeu Feedback###
![picture](/images/emailFeedback.JPG)

##11 - Email enviado para quem enviou o Feedback###
![picture](/images/respostaDoFeedback.JPG)

##12 - Email para o gerente semanalmente com os Feedbacks recebidos por seus subordinados###
![picture](/images/EmailDoGerente.JPG)