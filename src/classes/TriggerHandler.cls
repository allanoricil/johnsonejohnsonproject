public with sharing abstract class TriggerHandler{
	
	public List<SObject> newList;
	public List<SObject> oldList;
	public Map<Id, SObject> newMap;
	public Map<Id, SObject> oldMap;
	private System.TriggerOperation event;
	public List<SObject> recordsToInsert;
	public List<SObject> recordToDelete;
	public List<SObject> recordToUpdate;

	public TriggerHandler(System.TriggerOperation event,
						     List<SObject> triggerNew, 
                             List<SObject> triggerOld, 
                             Map<Id, SObject> triggerNewMap, 
                             Map<Id, SObject> triggerOldMap){
		this.event = event;
		newList = triggerNew;
		oldList = triggerOld;
		newMap = triggerNewMap;
		oldMap = triggerOldMap;
		recordsToInsert = new List<SObject>();
		recordToDelete = new List<SObject>();
		recordToUpdate = new List<SObject>();
	}
    
	public void execute(){
		switch on event{
			when BEFORE_UPDATE{
				System.debug('BEFORE UPDATE');
                loadDataForBeforeUpdate();
				for(SObject o : newList){
					onBeforeUpdate(o);
                }
			}
			when AFTER_INSERT{
				System.debug('AFTER INSERT');
                loadDataForAfterInsert();
				for(SObject o : newList){
					onAfterInsert(o);
                }
			}
			
			when BEFORE_INSERT{
				System.debug('BEFORE INSERT');
                loadDataForBeforeInsert();
                for(SObject o : newList){
					onBeforeInsert(o);
                }
			}
			/*
			when AFTER_UPDATE{
				System.debug('AFTER UPDATE');
                loadDataForAfterUpdate();
				for(SObject o : newList){
					onAfterUpdate(o);
                }
			}
			when AFTER_UNDELETE{
				System.debug('AFTER UNDELETE');
                loadDataForAfterUndelete();
				for(SObject o : newList){
					onAfterUndelete(o);
                }
			}
			when AFTER_DELETE{
				System.debug('AFTER DELETE');
                loadDataForAfterDelete();
				for(SObject o : oldList){
					onAfterDelete(o);
                }
			}
			when BEFORE_DELETE{
				System.debug('BEFORE DELETE');
                loadDataForBeforeDelete();
				for(SObject o : oldList){
					onBeforeDelete(o);
                }
			}*/
		}

		postTriggerEventsDmlActions();
	}

	private void postTriggerEventsDmlActions(){
		if(!recordsToInsert.isEmpty()) Database.insert(recordsToInsert, true);
		if(!recordToUpdate.isEmpty()) Database.update(recordToUpdate, true);
		if(!recordToDelete.isEmpty()) Database.delete(recordToDelete, true);
	}

	public abstract void onBeforeInsert(SObject o);
	public abstract void onBeforeUpdate(SObject o);
	public abstract void onAfterInsert(SObject o);
    public abstract void loadDataForBeforeUpdate();
    public abstract void loadDataForAfterInsert();
    public abstract void loadDataForBeforeInsert();

    /*
	
	public abstract void onBeforeDelete(SObject o);
	public abstract void onAfterUpdate(SObject o);
	public abstract void onAfterDelete(SObject o);
	public abstract void onAfterUndelete(SObject o);
   	
    public abstract void loadDataForAfterUpdate();
    public abstract void loadDataForAfterUndelete();
    public abstract void loadDataForAfterDelete();
    public abstract void loadDataForBeforeDelete();*/
}