@isTest
public class UserDataFactory {
	
	private static Map<String, Profile> PROFILES_MAP;
	private static Map<String, UserRole> ROLES_MAP;

	static{
		List<Profile> profiles = [SELECT Id, Name FROM Profile];
		for(Profile profile : profiles){
			if(PROFILES_MAP	== null){
				PROFILES_MAP = new Map<String, Profile>();
			}
			PROFILES_MAP.put(profile.Name, profile);
		}

		List<UserRole> UserRoles = [SELECT Id, DeveloperName FROM UserRole];
		for(UserRole userRole : UserRoles){
			if(ROLES_MAP	== null){
				ROLES_MAP = new Map<String, UserRole>();
			}
			ROLES_MAP.put(userRole.DeveloperName, userRole);
		}
	}

	public static User createManager(String lastname){
		return	createUser('manager', 
				lastname, 
				lastname + '@john.com.br', 
				lastname + '@john.com.br', 
				'manager', 
				'Manager',
				'Manager');
	}

	public static User createUser(String lastname){
		return	createUser('user', 
				lastname,
				lastname + '@john.com.br', 
				lastname + '@john.com.br', 
				'user',
				'User',
				'User');
	}

	public static User createSubuser(String lastname){
		return	createUser('subuser', 
				lastname,
				lastname + '@john.com.br', 
				lastname + '@john.com.br', 
				'subuser',
				'Subuser',
				'Subuser');
	}

	private static User createUser(String firstName,
		                           String lastName, 
		                           String email, 
		                           String username,
		                           String alias,
		                           String profileName,
		                           String roleName){
			User user = new User();
			if(PROFILES_MAP.containsKey(profileName)){
				user.ProfileId = PROFILES_MAP.get(profileName).Id;
			}else{
				user.ProfileId = PROFILES_MAP.get('System Administrator').Id;
			}
			if(ROLES_MAP.containsKey(roleName)) {
				user.UserRoleId = ROLES_MAP.get(roleName).Id;
			}
			user.Username = username;
			user.Firstname = firstname;
			user.Lastname = lastname;
			user.Email = email;
			user.Alias = alias;
			user.TimeZoneSidKey = 'America/Sao_Paulo';
			user.LocaleSidKey = 'pt_BR';
			user.EmailEncodingKey = 'ISO-8859-1';
			user.LanguageLocaleKey = 'pt_BR';
			user.isActive = true;

			return user;
	}
}