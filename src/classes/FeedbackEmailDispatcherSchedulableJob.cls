global class FeedbackEmailDispatcherSchedulableJob implements Schedulable {
	

	global void execute(SchedulableContext sc) {
		FeedbackEmailDispatcherJob batch = new FeedbackEmailDispatcherJob();
		database.executebatch(batch);
	}
}