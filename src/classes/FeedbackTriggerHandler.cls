public with sharing class FeedbackTriggerHandler extends TriggerHandler{
	
	private Map<Id, Id> managersMap;
	private Map<Id, ProcessInstance> processInstancesMap;
	private Map<Id, Feedback__c> feedbacksWithProcessInstancesMap;


	public FeedbackTriggerHandler(System.TriggerOperation event,
									 List<Feedback__c> triggerNew, 
		                             List<Feedback__c> triggerOld, 
		                             Map<Id, Feedback__c> triggerNewMap, 
		                             Map<Id, Feedback__c> triggerOldMap){
		super(event, triggerNew, triggerOld, triggerNewMap, triggerOldMap);
		managersMap = new Map<Id, Id>();
    }

    public override void onBeforeInsert(SObject so){
    	//Fiz isso pensando em uma carga em massa. Imagina que alguem queira
    	//carregar feedbacks de outro sistema para o SF, entao o proprietario
    	//deve ser o usuario escolhido no campo Feedback_From__c.
    	Feedback__c newFeedback = (Feedback__c)so;
    	newFeedback.OwnerId = newFeedback.Feedback_From__c;
    }
	
	public override void onBeforeUpdate(SObject so){
		Feedback__c newFeedback  = (Feedback__c)so;
		ProcessInstance processInstanceForThisFeedback = null;
		if(processInstancesMap != null){
			for(ProcessInstance pi : feedbacksWithProcessInstancesMap.get(newFeedback.Id).ProcessInstances){
				if(processInstancesMap.containsKey(pi.Id)){
					processInstanceForThisFeedback = processInstancesMap.get(pi.Id);
					break;
				}
			}

			if(processInstanceForThisFeedback != null){
				String comments = processInstanceForThisFeedback.Steps.get(0).comments;
				if(comments == null && processInstanceForThisFeedback.Status.equals('Rejected')){
					newFeedback.addError('It is necessary to add a Comment if you are rejecting this Feedback');
				}else{
					newFeedback.Comments_Response__c = comments;
				}
			}
		}
	}
	
	public override void onAfterInsert(SObject so){
		Feedback__c newFeedback = (Feedback__c)so;

		//share with the To User
		if(UserInfo.getUserId() != newFeedback.Feedback_To__c){
			recordsToInsert.add(FeedbackHelper.createReadAcessToTheRecordForTheUser(newFeedback.Id, newFeedback.Feedback_To__c));
		}
		
		//share with the feedback receiver's Manager
		if(managersMap.containsKey(newFeedback.Feedback_To__c) && UserInfo.getUserId() != managersMap.get(newFeedback.Feedback_To__c)){
			recordsToInsert.add(FeedbackHelper.createReadAcessToTheRecordForTheUser(newFeedback.Id, managersMap.get(newFeedback.Feedback_To__c)));
		}
		
	}

    public override void loadDataForAfterInsert(){
    	List<Id> feedbackReceiverUsersIds = new List<Id>();
    	for(Feedback__c feedback : (List<Feedback__c>)newList){
    		feedbackReceiverUsersIds.add(feedback.Feedback_To__c);
    	}
    	List<User> users = [SELECT Id, 
    							   ManagerId,
    							   Manager.isActive 
    						  FROM User 
    						 WHERE Id IN:feedbackReceiverUsersIds 
    						       AND ManagerId <> null
    						       AND Manager.isActive  = true];
    	for(User user : users){
    		managersMap.put(user.Id, user.ManagerId);
    	}
    }

    public override void loadDataForBeforeUpdate(){
    	feedbacksWithProcessInstancesMap = new Map<Id, Feedback__c>([SELECT Id,
																	 (SELECT Id
																		FROM ProcessInstances) 
																FROM Feedback__c 
															   WHERE Id IN :newMap.keySet()]);
    	
    	processInstancesMap = new Map<Id, ProcessInstance>([SELECT Id,
		    												     Status,
		    												     TargetObjectId,
		    												     (SELECT Id, 
		    												             StepStatus, 
		    												             Comments 
		    												        FROM Steps
		    												       ORDER BY CreatedDate DESC) 
		    												FROM ProcessInstance 
		    											   WHERE TargetObjectId IN :newMap.keySet()]);
    }
	
	public override void loadDataForBeforeInsert(){}
    /*
	public override void onBeforeDelete(SObject so){}
	public override void onAfterUpdate(SObject so){}
	public override void onAfterDelete(SObject so){}
	public override void onAfterUndelete(SObject so){}
    public override void loadDataForAfterUpdate(){}
    public override void loadDataForAfterUndelete(){}
    public override void loadDataForAfterDelete(){}
    public override void loadDataForBeforeDelete(){}*/
}