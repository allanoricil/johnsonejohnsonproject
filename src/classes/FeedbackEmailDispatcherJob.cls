global class FeedbackEmailDispatcherJob implements Database.Batchable<sObject> {
	
	private Map<Id, List<Feedback__c>> feedbacksByManagerIdMap;

	public FeedbackEmailDispatcherJob(){
		//query feedbacks of the current month
		List<Feedback__c> feedbacks = [SELECT id, 
											  Feedback_To__r.Name,
											  Feedback_From__r.Name,
											  Feedback_To__r.Manager.Email,
											  Feedback_To__r.ManagerId,
											  CreatedDate 
									     FROM Feedback__c 
									    WHERE Feedback_To__r.ManagerId <> null
									      AND CreatedDate = THIS_MONTH
									ORDER BY Feedback_To__r.ManagerId];

		//map feedbacks by manager
	  	feedbacksByManagerIdMap = new Map<Id, List<Feedback__c>>();
		for(Feedback__c feedback : feedbacks){
			if(!feedbacksByManagerIdMap.containsKey(feedback.Feedback_To__r.Manager.Id)){
				List<Feedback__c> feedbacksForThisManager = new List<Feedback__c>();
				feedbacksForThisManager.add(feedback);
				feedbacksByManagerIdMap.put(feedback.Feedback_To__r.Manager.Id, feedbacksForThisManager);
			}else{
				feedbacksByManagerIdMap.get(feedback.Feedback_To__r.Manager.Id).add(feedback);
			}
		}
	}


	global Database.QueryLocator start(Database.BatchableContext BC) {
		//query the users that have a manager.
		return Database.getQueryLocator('SELECT Id, ManagerId, Manager.Email, Manager.Name FROM User WHERE isActive = true AND ManagerId <> null');
	}

   	global void execute(Database.BatchableContext BC, List<User> scope) {
		List<Messaging.SingleEmailMessage> emailsToBeSent = new List<Messaging.SingleEmailMessage>();
	   	for(User manager : scope){
	   		if(feedbacksByManagerIdMap.containsKey(manager.ManagerId)){
	   			List<Feedback__c> feedbacksForThisManager = feedbacksByManagerIdMap.get(manager.ManagerId);

				//build the email with all the feedbacks Ids
				String body = 'Dear ' + manager.Manager.Name + ',</br></br>'  + 
				              'In the list below you will find the Feedbacks Received by your Subordinates:</br></br>';

			    for(Feedback__c feedback : feedbacksForThisManager){
			    	body+= '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + feedback.Id + '">'+ 
			    			'De ' + feedback.Feedback_From__r.Name + 
			    			' Para ' + feedback.Feedback_To__r.Name + 
			    			' no dia ' + feedback.CreatedDate +'</a></br>';	
			    }

			    //save the email
			    List<String> recipients = new List<String>();
			    recipients.add(manager.Manager.Email);
			    emailsToBeSent.add(EmailHelper.buildEmailToRecipients(recipients, 'Feedbacks received by your Subordinates', body));
			}
   		}

   		//send the emails
   		if(!emailsToBeSent.isEmpty()){
   			List<Messaging.SendEmailResult> results = EmailHelper.sendEmailToRecipients(emailsToBeSent);
   			for(Messaging.SendEmailResult emailResult : results){
   				if(!emailResult.isSuccess()){
   					throw new EmailException();
   				}
   			}
   		}
	}
	
	global void finish(Database.BatchableContext BC){}
	
}