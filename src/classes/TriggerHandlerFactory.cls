public with sharing abstract class TriggerHandlerFactory {
	


	public static TriggerHandler newHandler(String name,
											System.TriggerOperation event,
											List<SObject> triggerNew, 	
                             				List<SObject> triggerOld, 
                             				Map<Id, SObject> triggerNewMap, 
                             				Map<Id, SObject> triggerOldMap){		
		if(name != null){
			if(name.equals('Feedback__c')){
				return new FeedbackTriggerHandler(event,
												 (List<Feedback__c>)triggerNew, 
												 (List<Feedback__c>)triggerOld, 
												 (Map<Id,Feedback__c>)triggerNewMap, 
												 (Map<Id,Feedback__c>)triggerOldMap);
			}
		}
		return null;
	}


}