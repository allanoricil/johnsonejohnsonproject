@isTest
public class FeedbackDataFactory {
	
	
	public static Feedback__c createFeedback(User feedbackFromUser, 
											 User feedbackToUser, 
											 String communicationSkills, 
											 String commitment, 
											 String leadershipSkills){

		Feedback__c feedback = new Feedback__c();
		feedback.Feedback_From__c = feedbackFromUser.Id;
		feedback.Feedback_To__c = feedbackToUser.Id;
		feedback.Communication_Skills__c = communicationSkills;
		feedback.Commitment__c = commitment;
		feedback.Leadership_Skills__c = leadershipSkills;
		return feedback;
	}


}