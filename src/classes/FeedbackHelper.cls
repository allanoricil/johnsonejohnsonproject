public abstract with sharing class FeedbackHelper {
	

	public static Feedback__Share createReadAcessToTheRecordForTheUser(String feedbackId, Id userId){
		Feedback__Share readAcess = new Feedback__Share();
		readAcess.ParentId = feedbackId;
		readAcess.UserOrGroupId = userId;
		readAcess.AccessLevel = 'Read';
		return readAcess;
	}

}