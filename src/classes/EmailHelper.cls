public abstract without sharing class EmailHelper {
	
	public static Messaging.SingleEmailMessage buildEmailToRecipients(List<String> recipients, String subject, String htmlBody){
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.setSubject(subject);
		message.setHtmlBody(htmlBody);
		message.setToAddresses(recipients);
		return message;
	}


	public static List<Messaging.SendEmailResult> sendEmailToRecipients(List<Messaging.SingleEmailMessage> emails){
		return Messaging.SendEmail(emails);
	}
}