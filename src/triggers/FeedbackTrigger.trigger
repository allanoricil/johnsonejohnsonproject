trigger FeedbackTrigger on Feedback__c (before insert, before update, after insert) {

	TriggerHandler FeedbackTriggerHandler = TriggerHandlerFactory.newHandler('Feedback__c',
									                                         Trigger.operationType,
									                                         Trigger.new, 
									                                         Trigger.old, 
									                                         Trigger.newMap, 
									                                         Trigger.oldMap);
    
    FeedbackTriggerHandler.execute();

}