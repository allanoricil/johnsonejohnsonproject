<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Feedback_Response</fullName>
        <description>Feedback Response</description>
        <protected>false</protected>
        <recipients>
            <field>Feedback_From__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Feedback_Response</template>
    </alerts>
    <alerts>
        <fullName>Feedback_Sent</fullName>
        <description>Feedback Sent</description>
        <protected>false</protected>
        <recipients>
            <field>Feedback_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Feedback_Given</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Feedback_Acceptance_to_No</fullName>
        <field>Feedback_Acceptance__c</field>
        <literalValue>No</literalValue>
        <name>Update Feedback Acceptance to No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Feedback_Acceptance_to_Yes</fullName>
        <field>Feedback_Acceptance__c</field>
        <literalValue>Yes</literalValue>
        <name>Update Feedback Acceptance to Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
